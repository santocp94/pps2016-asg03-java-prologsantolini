package u07asg;

import java.util.List;
import java.util.Optional;

enum Player {RedPlayer, YellowPlayer}

public interface Connect4 {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean getCell(int i);

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);

    int countRedMoves();

    int countYellowMoves();
}
