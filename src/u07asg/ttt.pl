% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A1,B1,C1,D1,E1,F1,A2,B2,C2,D2,E2,F2,
A3,B3,C3,D3,E3,F3,A4,B4,C4,D4,E4,F4,A5,B5,C5,D5,E5,F5,
A6,B6,C6,D6,E6,F6,A7,B7,C7,D7,E7,F7]),
print_row(A1,B1,C1,D1,E1,F1),print_row(A2,B2,C2,D2,E2,F2),print_row(A3,B3,C3,D3,E3,F3),
print_row(A4,B4,C4,D4,E4,F4),print_row(A5,B5,C5,D5,E5,F5),print_row(A6,B6,C6,D6,E6,F6).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C,D,E,F) :- put(A),put(' '),put(B),put(' '),put(C),put(' '),put(D),put(' '),put(E),put(' '),put(F),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(36,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,even) :- not(member(null,B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% columns
% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x,o]).
finalpatt([o,o,o,o,o,o,x,x,x,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,x,x,x]).

%rows
finalpatt([x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o]).
finalpatt([o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o]).
finalpatt([o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o]).
finalpatt([o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o]).
finalpatt([o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o]).
finalpatt([o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x,o,o,o,o,o,x]).

%diagonal
finalpatt([x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,x]).
finalpatt([o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o]).
finalpatt([o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o,x,o,o,o,o,o,o]).

%diagonal2
finalpatt([o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o]).
finalpatt([o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,o,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o]).
finalpatt([o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o,o,x,o,o,o]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).

%count_moves(+Board, -RedMoves, -YellowMoves): counts the number of moves maid by the two players
count_moves([],0,0).
count_moves([p1|T],N,M) :- !,count_moves(T,N1,M),N is N1+1.
count_moves([p2|T],N,M) :- !,count_moves(T,N,N2),M is N2+1.
count_moves([_|T],N,M) :- !,count_moves(T,N,M).