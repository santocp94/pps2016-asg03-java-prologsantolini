package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** Connect4 application
 */
public class Connect4App {

    private final Connect4 ttt;
    private final JButton[][] board = new JButton[6][6];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("Connect4");
    private boolean finished = false;
    private Player turn = Player.RedPlayer;
    private int moves = 0;

    private void changeTurn(){
        this.turn = this.turn == Player.RedPlayer ? Player.YellowPlayer : Player.RedPlayer;
    }

    public Connect4App(Connect4 ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }

    private void humanMove(int i, int j){

        if(i != 5 && !ttt.getCell((i+1) + 6 * j)){
            System.out.println("Move NOT allowed");
        }else{
            if (ttt.move(turn,i,j)){
                board[i][j].setBackground(turn == Player.RedPlayer ? Color.red : Color.yellow);
                moves++;
                System.out.println("Red player moves: " + ttt.countRedMoves());
                System.out.println("Yellow player moves: " + ttt.countYellowMoves());
                System.out.println("");
                changeTurn();
                if (moves > 32){
                    System.out.println("X winning count: "+ttt.winCount(turn, Player.RedPlayer));
                    System.out.println("O winning count: "+ttt.winCount(turn, Player.YellowPlayer));
                }
            }
            Optional<Player> victory = ttt.checkVictory();
            if (victory.isPresent()){
                exit.setText(victory.get()+" won!");
                System.out.println("Player won in " + (victory.get() == Player.RedPlayer ?  ttt.countRedMoves() :  ttt.countYellowMoves()) + " moves!");
                finished=true;
                return;
            }
            if (ttt.checkCompleted()){
                exit.setText("Even!");
                finished=true;
                return;
            }
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(6,6));
        for (int i=0;i<6;i++){
            for (int j=0;j<6;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(300,320);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            new Connect4App(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
            e.printStackTrace();
        }
    }
}
